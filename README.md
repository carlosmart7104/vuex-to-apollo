# Vuex -> Apollo migration with Rick and Morty

This is a small proof-of-concept project to research different strategies of migrating an existing Vuex-based app with some REST API calls to use GraphQL API
Please leave comments on existing RFC: https://gitlab.com/gitlab-org/frontend/rfcs/issues/7

## What's the application?

This application fetches two different lists related to _Rick and Morty_ series: episodes list and characters list. In the 'original' application both lists are fetched on the app creation and stored to Vuex. We also have an ability to add or remove characters in a favorites list. This favorites list is stored in Vuex as well and belongs to the local application state. Favorite characters are rendered on the respective route.

When fetching the data from the API, we set `isLoading` Vuex property to `true`.

In the 'original' application all data is fetched from the REST endpoint via **axios**.

You can find this application code in [`master`](https://gitlab.com/ntepluhina/vuex-to-apollo/tree/master) branch.

## Let's change to GraphQL!

Now imagine we already have a GraphQL endpoint to fetch episodes, but characters will still come through REST API. We need to make changes to our app to use both endpoints, knowing that _**in the future all data will come from GraphQL endpoint**_. So we need to consider not only a migration to 'hybrid' app, but also a migration from 'hybrid' to 'full GraphQL'.

There are two main paths to perform this migration: perform GraphQL calls in Vuex actions and store response in Vuex with disabling Apollo Cache (single source of truth: Vuex) or ditch Vuex and store everything in Apollo Client cache. In this case Apollo cache will serve as a single source of truth, storing not only GraphQL API calls results, but also the local state and any REST API response.

### Vuex with GraphQL path

You can find a finished source code for this path in [`vuex-graphql`](https://gitlab.com/ntepluhina/vuex-to-apollo/tree/vuex-graphql) branch.

Diffs with `master` can be found [here](https://gitlab.com/ntepluhina/vuex-to-apollo/merge_requests/1/diffs)

When including GraphQL API calls to Vuex, first we need to install all the dependencies we need:

```bash
yarn add graphql apollo-client apollo-link apollo-link-http apollo-cache-inmemory graphql-tag
```

Please notice we're not using `vue-apollo` integration here, as we won't use Apollo queries in components. We don't need it yet.

All code lives inside the `src` folder, so all paths in this guide are releative to that.

Let's kick it off by creating a fresh `graphql` folder.
This will house all files dedicated to graphql operations, like queries or mutations.
Let's create a `graphql/client.js` file and set up Apollo client there:

```js
// graphql/client.js

import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

// Here we define the ling to our GraphQL endpoint
const httpLink = createHttpLink({
  uri: 'https://rickandmortyapi.com/graphql'
});

// Even though we're not going to use Apollo cache at all, we still need to provide it when creating Apollo Client instance
const cache = new InMemoryCache();

export const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
  // We disable the Apollo cache here because we use Vuex as a single source of truth
  defaultOptions: {
    query: {
      fetchPolicy: 'network-only'
    }
  }
});
```

We want to use `.gql` files to store our queries and mutations, so we need to add a proper loader to webpack config in `vue.config.js` file:

> Note: The `vue.config.js` file needs to live in the root directory of the project, not the `src` folder.

```js
// vue.config.js

module.exports = {
  chainWebpack: config => {
    config.module
      .rule('graphql')
      .test(/\.(graphql|gql)$/)
      .use('graphql-tag/loader')
      .loader('graphql-tag/loader')
      .end();
  }
};
```

Now, we need to specify the GraphQL query to fetch episodes. The GraphQL schemas can be found [here](https://rickandmortyapi.com/graphql/). Let's create `graphql/episodes.query.gql` file and store the query there:

```js
// graphql/episodes.query.gql

query Episodes {
  episodes {
    results {
      id
      name
      air_date
      episode
    }
  }
}
```

We're ready to fetch our data! Let's import `apolloClient` and `episodesQuery` to `store/index.js` file:

```js
// store/index.js

import { apolloClient } from '../graphql/client';
import episodesQuery from '../graphql/queries/episodes.query.gql';
```

Now, in `getEpisodes` action we simply replace the `axios` call with our new Apollo Client query:

```js
// store/index.js
getEpisodes({ commit }) {
  commit('toggleLoading', true);
  apolloClient
    .query({
      query: episodesQuery
    })
    .then(res => commit('getEpisodes', res.data.episodes.results))
    .catch(err => commit('setError', err))
    .finally(() => commit('toggleLoading', false));
},
```

That's it! We have episodes fetched from the GraphQL API now. Literally a 20 minutes adventure!

#### Pros

- does not require significant code changes;
- does not require `vue-apollo`
- there is 'no magic', we handle loading and error states by ourselves;

#### Cons

- we limit Apollo Client functionality to make is just another axios-like library (in particular, we disable Apollo cache);
- while `loading` and `error` states (and potentially polling) could be handled automatically by Apollo Client, we reinvent the wheel doing this by hand in Vuex;
- the migration from 'hybrid' app to 'full GraphQL' might take longer now, as we've decided we use Apollo Client and no Vuex in GraphQL-only applications

### Wrapping everything with Apollo Client

You can find a finished source code for this path in [`apollo-client`](https://gitlab.com/ntepluhina/vuex-to-apollo/tree/apollo-client) branch.

Diffs with `master` can be found [here](https://gitlab.com/ntepluhina/vuex-to-apollo/merge_requests/2/diffs)

In this case we'll remove Vuex completely and wrap everything to be stored in the Apollo Cache. We will need to take all the steps described in the [previous path](#vuex-with-graphql-path) with some changes described below.

As a first step, we need to define all the data we need to get from the cache and all the ways we can change this data. 
We need to fetch 3 different lists:

- episodes;
- characters;
- favorite characters

These lists will be fetched with 3 GraphQL queries. Currently we have one for `episodes`; let's create two more.
As a matter of good practice and structure, we'll move all queries and mutations into a `queries` subfolder.

```js
// graphql/queries/characters.query.gql

query Characters {
  characters @client {
    id
    name
    location
    image
  }
}
```

```js
// graphql/queries/favoriteCharacters.query.gql

query FavoriteCharacters {
  favoriteCharacters @client {
    id
    name
    location
    image
  }
}
```

> Please note the `@client` directive! It means these two queries will be run locally, without accessing GraphQL endpoint. In fact, `characters` query will fetch data from REST API but we'll cover that later

And similarly to `addToFavorites` and `removeFromFavorites` mutations in Vuex we will have 2 _local_ mutations:

```js
// graphql/queries/addToFavorites.mutation.gql

mutation AddToFavorites($character: Character!) {
  addToFavorites(character: $character) @client {
    id
    name
    image
    location
  }
}
```

```js
// graphql/queries/removeFromFavorites.mutation.gql

mutation RemoveFromFavorites($id: ID!) {
  removeFromFavorites(id: $id) @client
}
```

Again, both of them have `@client` directive, meaning we're going to change local data.

Now we need to 'tell' our Apollo Client how to handle these local queries and mutations: we need to write local resolvers and seed our cache with initial data (i.e. empty `favoriteCharacters` array).

Let's open `graphql/client.js` and seed initial data:

```js
// graphql/client.js

const cache = new InMemoryCache();

cache.writeData({ data: { favoriteCharacters: [] } });
```

Now we can simply query `favoriteCharacters` as-is, without adding any local resolvers. But it's not the case for `charaters`: we need to specify where are we going to get characters list. Let's create a `resolver` for it:

```js
// graphql/client.js
// Let's add axios to our imports
import axios from 'axios'

// And tell axios about the default uri to fetch data from
axios.defaults.baseURL = 'https://rickandmortyapi.com/api/';

const resolvers = {
  Query: {
    characters() {
      // We fetch data from REST API and do some basic transformation to match the properties of Characters query
      return axios.get('/character').then(res =>
        res.data.results.map(char => ({
          __typename: 'Character',
          id: char.id,
          name: char.name,
          location: {
            name: char.location.name
          },
          image: char.image
        }))
      );
    }
  }
};

export const apolloClient = new ApolloClient({
  link: httpLink,
  cache,
  // Don't forget to add resolvers to your Apollo Client!
  resolvers
});
```

Now, when our component will try to fetch `characters`, the resolver will fetch the data from REST API, shape them and provide as a response to GraphQL query.

We also need to define resolvers for local mutations:

```js
// graphql/client.js
import favoriteCharactersQuery from './queries/favoriteCharacters.query.gql';

const resolvers = {
  Query: {...},
  Mutation: {
    addToFavorites(_, { character }, { cache }) {
      // We read the query from the cache, add the passed character and write query back
      const data = cache.readQuery({ query: favoriteCharactersQuery });
      data.favoriteCharacters = [...data.favoriteCharacters, character];
      cache.writeQuery({ query: favoriteCharactersQuery, data });
    },
    removeFromFavorites(_, { id }, { cache }) {
      // We read the query from the cache, remove the passed character by id and write query back
      const data = cache.readQuery({ query: favoriteCharactersQuery });
      data.favoriteCharacters = data.favoriteCharacters.filter(
        character => character.id !== id
      );
      cache.writeQuery({ query: favoriteCharactersQuery, data });
    }
  }
};
```

Now we're ready to use our queries and mutations in the Vue components. To do so, we need to install `vue-apollo` plugin:

`yarn add vue-apollo`

and to create an Apollo provider in `main.js`:

```js
// main.js
...
import VueApollo from 'vue-apollo';
import { apolloClient } from './graphql/client';

Vue.use(VueApollo);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});


new Vue({
  router,
  apolloProvider,
  render: h => h(App)
}).$mount('#app');
```

After this, we need to replace Vuex mappings in components with Apollo queries and mutations:

```js
// Episodes.vue

import episodesQuery from '../graphql/queries/episodes.query.gql';
export default {
  data() {
    return {
      episodes: [],
      fields: ['id', 'name', 'air_date', 'episode']
    }
  },
  apollo: {
    episodes: {
      query: episodesQuery
    }
  }
};
```

```js
// Characters.vue

import charactersQuery from '../graphql/queries/characters.query.gql';
import favoriteCharactersQuery from '../graphql/queries/favoriteCharacters.query.gql';
import addToFavoritesMutation from '../graphql/queries/addToFavorites.mutation.gql';
export default {
  data() {
    return {
      characters: [],
      favoriteCharacters: []
    }
  },
  apollo: {
    characters: {
      query: charactersQuery
    },
    favoriteCharacters: {
      query: favoriteCharactersQuery
    }
  },
  methods: {
    addToFavorites(character) {
      this.$apollo.mutate({
        mutation: addToFavoritesMutation,
        variables: { character }
      })
    },
    isInFavorites(character) {
      return this.favoriteCharacters.includes(character);
    }
  }
};
```

```js
// Favorites.vue

import favoriteCharactersQuery from '../graphql/queries/favoriteCharacters.query.gql';
import removeFromFavoritesMutation from '../graphql/queries/removeFromFavorites.mutation.gql';
export default {
    data() {
    return {
      favoriteCharacters: []
    };
  },
  apollo: {
    favoriteCharacters: {
      query: favoriteCharactersQuery
    }
  },
  methods: {
    removeFromFavorites(id) {
      this.$apollo.mutate({
        mutation: removeFromFavoritesMutation,
        variables: { id }
      });
    }
  }
};
```

Handling loading state is automatic, we just need to check `loading` for every respective query:

```html
<div v-if="$apollo.queries.characters.loading" class="spinner w-100">
  <b-spinner label="Spinning"></b-spinner>
</div>
```

#### Pros

- we don't need Vuex
- we handle loading/error states automatically
- further migration to 'GraphQL-only' app will be more smooth

#### Cons

- initial amount of changes is significantly bigger than in Vuex+GraphQL path
- resolvers syntax is verbose and might be a cognitive overload comparing to Vuex mutations
